package com.hopescork.Services;

import com.android.volley.VolleyError;

import org.json.JSONException;

/**
 * Created by Bilal Khan on 15/7/17.
 */
public interface ResponseListener {
    void onError(VolleyError error);

    void onSuccess(String string) throws JSONException;
}
