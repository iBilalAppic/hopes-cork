package com.hopescork.OtherClass;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.hopescork.R;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Bilal Khan on 12/8/17.
 */

public class UtilsClass {
    public static final String TAG = R.string.app_name + "";

    public static final int CLEAR_STACK_FLAG = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;


    public static boolean isNetworkConnected(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm.getActiveNetworkInfo() != null) {
            return true;
        } else {
            UtilsClass.showSingleDialog(mContext, mContext.getString(R.string.error), mContext.getString(R.string.internet_not_connected), mContext.getString(R.string.ok), null, false);
            return false;
        }

    }

    public static boolean isNetworkConnectedWithoutAlert(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm.getActiveNetworkInfo() != null) {
            return true;
        } else {
            return false;
        }

    }

    public static String get_IMeI(Context mContext) {
        TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        String Imei_no = telephonyManager.getDeviceId();
        return Imei_no;
    }

    public static void showLog(String log) {
        Log.e(TAG, log);
    }

    public static int dpToPx(Context mContext, int dimenID) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, mContext.getResources().getDimension(dimenID), mContext.getResources().getDisplayMetrics());
    }

    public static void showToast(Context mContext, String message) {
        try {

            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

        }
    }

    public static AlertDialog.Builder showDialog(Context mContext, String message, String title, String positiveText, DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener, String negativeText, String neutralText, DialogInterface.OnClickListener neutralListener, boolean cancellable) {
        AlertDialog.Builder alert = getSimpleDialog(mContext);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setCancelable(cancellable);
        alert.setNegativeButton(negativeText, negativeListener);
        alert.setPositiveButton(positiveText, positiveListener);
        alert.setNeutralButton(neutralText, neutralListener);
        try {
            alert.show();
        } catch (Exception e) {
            UtilsClass.showLog("dialog exception " + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return alert;
    }

    public static AlertDialog.Builder getSimpleDialog(Context mContext) {
        return new AlertDialog.Builder(mContext);
    }

    public static AlertDialog.Builder showSingleDialog(Context mContext, String title, String message, String positiveText, DialogInterface.OnClickListener positiveListener, boolean cancellable) {
        return showDialog(mContext, message, title, positiveText, positiveListener, null, null, null, null, cancellable);
    }

    public static AlertDialog.Builder showRetryDialog(Context mContext, DialogInterface.OnClickListener positiveListener, boolean cancellable) {
        return showDialog(mContext, mContext.getString(R.string.request_failed), mContext.getString(R.string.error), mContext.getString(R.string.retry), positiveListener, null, mContext.getString(R.string.cancel), null, null, cancellable);
    }

    public static boolean isNotNull(Context mContext, EditText... et) {

        boolean valid = true;
        for (EditText editText : et) {
            if (!validEdittext(editText, mContext)) {
                valid = false;
            }
        }
        if (!valid) {
            UtilsClass.showSingleDialog(mContext, mContext.getString(R.string.error), mContext.getString(R.string.empty_fields), mContext.getString(R.string.ok), null, true);
        }
        return valid;
    }

    public static boolean isTextViewNotNull(TextView et_reg_phone, Context mContext) {
        if (et_reg_phone.getText() == null || et_reg_phone.getText().toString().equals("")) {
            et_reg_phone.setHint(mContext.getString(R.string.required_field));
            et_reg_phone.setHintTextColor(Color.RED);
            return false;
        } else {
            return true;
        }
    }


    public static boolean validEdittext(EditText et_reg_phone, Context mContext) {
        if (et_reg_phone.getText() == null || et_reg_phone.getText().toString().equals("")) {
            et_reg_phone.setError(mContext.getString(R.string.required_field));
//            et_reg_phone.setHintTextColor(ContextCompat.getColor(mContext, R.color.red));
            return false;
        } else {
            return true;
        }
    }

    public static boolean isFileValid(Uri uri) {
        return uri.getLastPathSegment().endsWith("doc") || uri.getLastPathSegment().endsWith("docx") || uri.getLastPathSegment().endsWith("pdf");
    }

    public static boolean isGpsEnabled(Context mContext) {
        return ((LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    /* public static void setLoggedUser(Context mContext, User userPojo) {
        Gson gson = new Gson();
        TinyDB db = new TinyDB(mContext);

        db.putString(Constants.user_logged_in_str_key, gson.toJson(userPojo));
    }

    public static User getLoggedUser(Context mContext) {

        Gson gson = new Gson();
        User user;
        TinyDB db = new TinyDB(mContext);

        String userStr = db.getString(Constants.user_logged_in_str_key);

        user = gson.fromJson(userStr, User.class);

        return user;

    }

    public static boolean isUserLoggedIn(Context mContext) {

        boolean loggedIn = false;

        try {
            if (getLoggedUser(mContext) != null && getLoggedUser(mContext).getId() != null && !getLoggedUser(mContext).getId().isEmpty()) {
                loggedIn = true;
                return loggedIn;
            }
        } catch (Exception e) {
            showLog(e.getLocalizedMessage());
        }

        return loggedIn;
    }*/


    public static ProgressDialog getProgressDialog(Context mContext) {
        ProgressDialog pd = new ProgressDialog(mContext, R.style.MyProgressTheme);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.setIndeterminateDrawable(ContextCompat.getDrawable(mContext, R.drawable.custom_prgress_bar));
        pd.setCancelable(false);
        try {

            pd.show();
        } catch (Exception e) {
            UtilsClass.showLog(e.getLocalizedMessage());

        }

        return pd;
    }

    public static void showBackButton(final Context mContext, Toolbar toolbar) {
        AppCompatActivity activity = (AppCompatActivity) mContext;
        if (activity.getSupportActionBar() != null) {

            activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            final Drawable upArrow = ContextCompat.getDrawable(mContext, R.drawable.ic_navigation_arrow_back);

            activity.getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AppCompatActivity) mContext).onBackPressed();
            }
        });

    }

    public static boolean emailValid(Context mContext, String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);

        if (!matcher.matches()) {
            UtilsClass.showSingleDialog(mContext, mContext.getString(R.string.error), mContext.getString(R.string.invalid_email), mContext.getString(R.string.ok), null, true);
        }

        return matcher.matches();
    }

    public static void goToFragment(Context mContext, Fragment fragment, int container, boolean addtoBackstack) {
        android.support.v4.app.FragmentTransaction transaction = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();

//        if ()

        transaction.replace(container, fragment);

        if (addtoBackstack)
            transaction.addToBackStack(null);

        try {
            transaction.commit();
            hideKeyboard(((Activity) mContext).getCurrentFocus(), mContext);
        } catch (Exception e) {

        }

    }


    @TargetApi(Build.VERSION_CODES.KITKAT)
    @SuppressLint("NewApi")
    public static String getRealPathFromURI(Uri uri, Context mContext) {
        final boolean isKitKat = Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(mContext.getApplicationContext(), uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(mContext.getApplicationContext(), contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type))

                {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]
                        {
                                split[1]
                        };

                return getDataColumn(mContext.getApplicationContext(), contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(mContext.getApplicationContext(), uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
        //  return cursor.getString(column_index);
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static void hideKeyboard(View view, Context mContext) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void callIntent(Context mContext, String number) throws Exception {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + number));
        mContext.startActivity(intent);
    }

    public static void emailIntent(Context mContext, String email) throws Exception {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", email, null));
        mContext.startActivity(emailIntent);
    }

    public static void fullScreen(Context mContext) {
        ((AppCompatActivity) mContext).getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public static void shareIntent(Context mContext) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, mContext.getString(R.string.share_text));
        sendIntent.setType("text/plain");
        mContext.startActivity(sendIntent);
    }

    public static boolean isMyServiceRunning(Context mContext, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
            }
            return false;
        }
        return true;
    }

    public static void sendSms(Context mContext, String number, String message) {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setData(Uri.parse("sms:" + number));
        sendIntent.putExtra("sms_body", message);
        mContext.startActivity(sendIntent);

    }

    public static void loadImage(final Context mContext, final ImageView imageView, final int placeholder, Integer dataToLoad) {
        Glide.with(mContext).load(dataToLoad).placeholder(placeholder).into(new SimpleTarget<GlideDrawable>() {
            @Override
            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                imageView.setImageDrawable(resource);
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
                imageView.setImageDrawable(ContextCompat.getDrawable(mContext, placeholder));
            }
        });
    }

    public static void loadImage(final Context mContext, final ImageView imageView, final int placeholder, Uri dataToLoad) {
        Glide.with(mContext).load(dataToLoad).placeholder(placeholder).into(new SimpleTarget<GlideDrawable>() {
            @Override
            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                imageView.setImageDrawable(resource);
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
                imageView.setImageDrawable(ContextCompat.getDrawable(mContext, placeholder));
            }
        });
    }

    public static void loadImage(final Context mContext, final ImageView imageView, final int placeholder, byte[] dataToLoad) {
        Glide.with(mContext).load(dataToLoad).placeholder(placeholder).into(new SimpleTarget<GlideDrawable>() {
            @Override
            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                imageView.setImageDrawable(resource);
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
                imageView.setImageDrawable(ContextCompat.getDrawable(mContext, placeholder));
            }
        });
    }

    public static void loadImage(final Context mContext, final ImageView imageView, final int placeholder, String dataToLoad) {
        Glide.with(mContext).load(dataToLoad).placeholder(placeholder).into(new SimpleTarget<GlideDrawable>() {
            @Override
            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                imageView.setImageDrawable(resource);

            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
                imageView.setImageDrawable(ContextCompat.getDrawable(mContext, placeholder));
            }
        });
    }

    public static void loadImage(final Context mContext, final ImageView imageView, final int placeholder, File dataToLoad) {
        Glide.with(mContext).load(dataToLoad).placeholder(placeholder).into(new SimpleTarget<GlideDrawable>() {
            @Override
            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                imageView.setImageDrawable(resource);
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
                imageView.setImageDrawable(ContextCompat.getDrawable(mContext, placeholder));
            }
        });
    }

}
