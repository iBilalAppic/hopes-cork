package com.hopescork.OtherClass;

/**
 * Created by Bilal Khan on 15/7/17.
 */

public class AppConstant {

    public static String MainUrl = "http://13.126.36.218/BoutiqueWebService/ReportServices/Service.svc/";

    public static String Login = MainUrl + "GetUserDetail";

    public static String GetAssignList = MainUrl + "GetAgentAssignOrder";

    public static String HistoryList = MainUrl + "GetAgentAssignOrderHistory";
    public static String CompleteOrder = MainUrl + "AgentOrdercomplete";

    public static String userId = "userId";
    public static String userName = "name";
    public static String emailId = "email";

    public static final String isLogin = "login";


    public static String notification_key_message = "message";
    public static String notification_key_type = "type";
    public static String notification_key_name = "title";
    public static String notification_key_number = "number";
    public static String notification_key_server_id = "server_contactId";
    public static String notification_key_image_url = "image";


}
