package com.hopescork.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.hopescork.Adapter.HomeAdpater;
import com.hopescork.Model.AssignListModel;
import com.hopescork.OtherClass.AppConstant;
import com.hopescork.OtherClass.AppPreferance;
import com.hopescork.OtherClass.UtilsClass;
import com.hopescork.R;
import com.hopescork.Services.LocationUpdatesService;
import com.hopescork.Services.RequestGenerator;
import com.hopescork.Services.ResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    ImageView goHistory;
    TextView noData;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    HomeAdpater homeAdpater;
    AppPreferance appPreferance;
    String userIdd;
    private static final int PERMISSION_GPS_CODE = 12;
    String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CALL_PHONE};
    ArrayList<AssignListModel> list = new ArrayList<AssignListModel>();
    private static long back_pressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        appPreferance = new AppPreferance();
        userIdd = appPreferance.getPreferences(HomeActivity.this, AppConstant.userId);
        checkGPSPermission();

    }

    private void checkGPSPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!allPermissionsGiven()) {
                requestPermissions(permissions, PERMISSION_GPS_CODE);
            } else {
                init();
            }
        } else {
            init();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean allPermissionsGiven() {

        for (String permission : permissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_GPS_CODE) {
            if (grantResults.length > 0) {
                boolean permissionGranted = false;
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(HomeActivity.this, getString(R.string.permission_not_granted), Toast.LENGTH_SHORT).show();
                        break;
                    } else {
                        permissionGranted = true;
                    }
                }

                if (permissionGranted) {
                    init();
                }
            }
        }
    }

    private void init() {

        noData = (TextView) findViewById(R.id.noDataHistory);
        recyclerView = (RecyclerView) findViewById(R.id.homeRecycler);
        linearLayoutManager = new LinearLayoutManager(HomeActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        goHistory = (ImageView) findViewById(R.id.history);

        goHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, HistoryActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        startLocationUpdateService();
        getAssignedList();


    }


    private void startLocationUpdateService() {
        if (!UtilsClass.isMyServiceRunning(getApplicationContext(), LocationUpdatesService.class))
            startService(new Intent(getApplicationContext(), LocationUpdatesService.class));

    }

    private void getAssignedList() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("agentid", userIdd);

            Log.e("WorkList Req", jsonObject.toString());

            RequestGenerator.makePostRequest(HomeActivity.this, AppConstant.GetAssignList, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    Toast.makeText(HomeActivity.this, "Some Network Issue Occured", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    if (!string.equals("")) {
                        Log.e("WorkList Res", string);
                        JSONObject jsonObject1 = new JSONObject(string);
                        JSONArray jsonArray = jsonObject1.getJSONArray("GetAgentAssignOrderResult");

                        JSONObject response = jsonArray.getJSONObject(jsonArray.length() - 1);
                        JSONObject jsonObject3 = response.getJSONObject("Response");
                        String status = jsonObject3.getString("status");
                        String message = jsonObject3.getString("message");

                        if (status.equals("success")) {
                            recyclerView.setVisibility(View.VISIBLE);
                            noData.setVisibility(View.GONE);
                            list.clear();
                            for (int i = 0; i < jsonArray.length() - 1; i++) {
                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                                AssignListModel assignListModel = new AssignListModel();
                                assignListModel.setBillDate(jsonObject2.getString("BillDate"));
                                assignListModel.setCustomerName(jsonObject2.getString("CustomerName"));
                                assignListModel.setShopName(jsonObject2.getString("ShopName"));
                                assignListModel.setAddress(jsonObject2.getString("address"));
                                assignListModel.setBillamount(jsonObject2.getString("billamount"));
                                assignListModel.setBillno(jsonObject2.getString("billno"));
                                assignListModel.setDiscount(jsonObject2.getString("discount"));
                                assignListModel.setFlatNo(jsonObject2.getString("flatno"));
                                assignListModel.setLat(jsonObject2.getString("lat"));
                                assignListModel.setLng(jsonObject2.getString("lng"));
                                assignListModel.setNetamount(jsonObject2.getString("netamount"));
                                assignListModel.setOrderstatus(jsonObject2.getString("orderstatus"));
                                assignListModel.setShopId(jsonObject2.getString("shopid"));

                                list.add(assignListModel);
                            }


                            homeAdpater = new HomeAdpater(HomeActivity.this, list);
                            recyclerView.setAdapter(homeAdpater);
                        } else {
                            recyclerView.setVisibility(View.GONE);
                            noData.setVisibility(View.VISIBLE);
                            noData.setText(message);
                        }
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        if (back_pressed + 2000 > System.currentTimeMillis()) {

            finish();
            super.onBackPressed();
        } else

        {
            Snackbar snackbar = Snackbar.make(noData, "Press once again to close app.", Snackbar.LENGTH_SHORT);
            View snackbarView = snackbar.getView();
            TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbarView.setBackgroundColor(Color.DKGRAY);
            snackbar.show();
            back_pressed = System.currentTimeMillis();

        }

    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }
}

