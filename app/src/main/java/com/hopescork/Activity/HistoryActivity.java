package com.hopescork.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.hopescork.Adapter.HistoryAdapter;
import com.hopescork.Adapter.HomeAdpater;
import com.hopescork.Model.AssignListModel;
import com.hopescork.OtherClass.AppConstant;
import com.hopescork.OtherClass.AppPreferance;
import com.hopescork.R;
import com.hopescork.Services.RequestGenerator;
import com.hopescork.Services.ResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HistoryActivity extends AppCompatActivity {

    ImageView goHome;
    TextView noData;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    HistoryAdapter historyAdpater;
    AppPreferance appPreferance;
    String userIdd;
    ArrayList<AssignListModel> list = new ArrayList<AssignListModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        appPreferance = new AppPreferance();
        userIdd = appPreferance.getPreferences(HistoryActivity.this, AppConstant.userId);
        init();


    }

    private void init() {

        noData = (TextView) findViewById(R.id.noHistory);
        recyclerView = (RecyclerView) findViewById(R.id.historyRecycler);
        linearLayoutManager = new LinearLayoutManager(HistoryActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        goHome = (ImageView) findViewById(R.id.home);

        goHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HistoryActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                finish();
            }
        });

        getHisotryList();
    }

    private void getHisotryList() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("agentid", userIdd);

            Log.e("WorkList Req", jsonObject.toString());

            RequestGenerator.makePostRequest(HistoryActivity.this, AppConstant.HistoryList, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    Toast.makeText(HistoryActivity.this, "Some Network Issue Occured", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    if (!string.equals("")) {
                        JSONObject jsonObject1 = new JSONObject(string);
                        JSONArray jsonArray = jsonObject1.getJSONArray("GetAgentAssignOrderHistoryResult");

                        JSONObject response = jsonArray.getJSONObject(jsonArray.length() - 1);
                        JSONObject jsonObject3 = response.getJSONObject("Response");
                        String status = jsonObject3.getString("status");
                        String message = jsonObject3.getString("message");

                        if (status.equals("success")) {
                            recyclerView.setVisibility(View.VISIBLE);
                            noData.setVisibility(View.GONE);

                            list.clear();
                            for (int i = 0; i < jsonArray.length() - 1; i++) {
                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                                AssignListModel assignListModel = new AssignListModel();
                                assignListModel.setBillDate(jsonObject2.getString("BillDate"));
                                assignListModel.setCustomerName(jsonObject2.getString("CustomerName"));
                                assignListModel.setShopName(jsonObject2.getString("ShopName"));
                                assignListModel.setAddress(jsonObject2.getString("address"));
                                assignListModel.setBillamount(jsonObject2.getString("billamount"));
                                assignListModel.setBillno(jsonObject2.getString("billno"));
                                assignListModel.setDiscount(jsonObject2.getString("discount"));
                                assignListModel.setLat(jsonObject2.getString("lat"));
                                assignListModel.setLng(jsonObject2.getString("lng"));
                                assignListModel.setNetamount(jsonObject2.getString("netamount"));
                                assignListModel.setOrderstatus(jsonObject2.getString("orderstatus"));

                                list.add(assignListModel);

                            }
                            historyAdpater = new HistoryAdapter(HistoryActivity.this, list);
                            recyclerView.setAdapter(historyAdpater);

                        } else {
                            recyclerView.setVisibility(View.GONE);
                            noData.setVisibility(View.VISIBLE);
                            noData.setText("No History Found");
                        }
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(HistoryActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }
}
