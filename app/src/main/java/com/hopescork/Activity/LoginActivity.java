package com.hopescork.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.hopescork.OtherClass.AppConstant;
import com.hopescork.OtherClass.AppPreferance;
import com.hopescork.R;
import com.hopescork.Services.RequestGenerator;
import com.hopescork.Services.ResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Button signInBtn;
    EditText phoneNumber;
    AppPreferance appPreferance;
    String isLogin, name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        appPreferance = new AppPreferance();
        isLogin = appPreferance.getPreferences(LoginActivity.this, AppConstant.isLogin);
        name = appPreferance.getPreferences(LoginActivity.this, AppConstant.userName);
        if (name == null) {
            name.equals("");
        }

        if (!isLogin.equals("")) {
            Toast.makeText(LoginActivity.this, "Welcome Back " + name, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();

        } else {
            init();
        }
    }

    private void init() {
        phoneNumber = (EditText) findViewById(R.id.phoneNo);

        signInBtn = (Button) findViewById(R.id.signInBtn);


        signInBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (v == signInBtn) {
            if (phoneNumber.getText().toString().equals("")) {
                phoneNumber.setError("Phone Number Required");
            } else if (phoneNumber.getText().toString().length() < 10) {
                phoneNumber.setError("Valid Phone Number Required");
            } else {
                LoginService();
            }

        }

    }

    private void LoginService() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("phoneno", phoneNumber.getText().toString());

            Log.e("Login Req", jsonObject.toString());

            RequestGenerator.makePostRequest(LoginActivity.this, AppConstant.Login, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    Toast.makeText(LoginActivity.this, "Some Network Issue Occured", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(String string) throws JSONException {

                    if (!string.equals("")) {
                        Log.e("Login Res", string);
                        JSONObject jsonObject1 = new JSONObject(string);
                        JSONArray jsonArray = jsonObject1.getJSONArray("GetUserDetailResult");
                        JSONObject userDetails = jsonArray.getJSONObject(0);

                        JSONObject response = jsonArray.getJSONObject(jsonArray.length() - 1);
                        JSONObject jsonObject3 = response.getJSONObject("Response");
                        String status = jsonObject3.getString("status");
                        String message = jsonObject3.getString("message");

                        if (status.equals("success")) {

                            appPreferance.setPreferences(LoginActivity.this, AppConstant.isLogin, "login");
                            appPreferance.setPreferences(LoginActivity.this, AppConstant.userId, userDetails.getString("userid"));
                            appPreferance.setPreferences(LoginActivity.this, AppConstant.userName, userDetails.getString("fname"));
                            appPreferance.setPreferences(LoginActivity.this, AppConstant.emailId, userDetails.getString("emailid"));
                            Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
