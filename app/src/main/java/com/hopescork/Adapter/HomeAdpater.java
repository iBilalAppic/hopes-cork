package com.hopescork.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hopescork.Activity.HomeActivity;
import com.hopescork.Activity.TrackActivity;
import com.hopescork.Model.AssignListModel;
import com.hopescork.R;

import java.util.ArrayList;

/**
 * Created by Bilal Khan on 3/7/17.
 */

public class HomeAdpater extends RecyclerView.Adapter<HomeAdpater.ViewHolder> {
    Context context;
    ArrayList<AssignListModel> list = new ArrayList<AssignListModel>();

    public HomeAdpater(HomeActivity homeActivity, ArrayList<AssignListModel> list) {

        this.context = homeActivity;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_home_card, parent, false);
        return new HomeAdpater.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AssignListModel assignListModel = list.get(position);
        holder.shopname.setText("Name : " + assignListModel.getCustomerName());
        holder.orderId.setText("Order Id : " + assignListModel.getBillno());
        holder.shopAdd.setText("Address : " + assignListModel.getFlatNo() + " " + assignListModel.getAddress());
        holder.amount.setText("Total Amount : " + context.getText(R.string.Rs) + " " + assignListModel.getNetamount());
        holder.status.setText(assignListModel.getOrderstatus());
        holder.status.setTextColor(Color.parseColor("#27AD5F"));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView shopname, orderId, shopAdd, amount, status;

        public ViewHolder(View itemView) {
            super(itemView);

            shopname = (TextView) itemView.findViewById(R.id.shopName);
            orderId = (TextView) itemView.findViewById(R.id.orderId);
            shopAdd = (TextView) itemView.findViewById(R.id.shopAddress);
            amount = (TextView) itemView.findViewById(R.id.amount);
            status = (TextView) itemView.findViewById(R.id.orderStatus);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            if (view == itemView) {

                Intent intent = new Intent(context, TrackActivity.class);
                intent.putExtra("orderid", list.get(getAdapterPosition()).getBillno());
                intent.putExtra("shopid", list.get(getAdapterPosition()).getShopId());
                intent.putExtra("lat", list.get(getAdapterPosition()).getLat());
                intent.putExtra("lan", list.get(getAdapterPosition()).getLng());
                context.startActivity(intent);
            }
        }
    }
}
