package com.hopescork.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hopescork.Activity.HistoryActivity;
import com.hopescork.Model.AssignListModel;
import com.hopescork.R;

import java.util.ArrayList;

/**
 * Created by Bilal Khan on 15/7/17.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {


    Context context;
    ArrayList<AssignListModel> list = new ArrayList<AssignListModel>();

    public HistoryAdapter(HistoryActivity historyActivity, ArrayList<AssignListModel> list) {

        this.context = historyActivity;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_home_card, parent, false);
        return new HistoryAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AssignListModel assignListModel = list.get(position);
        holder.shopname.setText("Name : " + assignListModel.getCustomerName());
        holder.orderId.setText("Order Id : " + assignListModel.getBillno());
        holder.shopAdd.setText("Address : " + assignListModel.getAddress());
        holder.amount.setText("Total Amount : " + assignListModel.getNetamount());
        holder.status.setText(assignListModel.getOrderstatus());
        holder.status.setTextColor(Color.parseColor("#ff0101"));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        private TextView shopname, orderId, shopAdd, amount, status;

        public ViewHolder(View itemView) {
            super(itemView);

            shopname = (TextView) itemView.findViewById(R.id.shopName);
            orderId = (TextView) itemView.findViewById(R.id.orderId);
            shopAdd = (TextView) itemView.findViewById(R.id.shopAddress);
            amount = (TextView) itemView.findViewById(R.id.amount);
            status = (TextView) itemView.findViewById(R.id.orderStatus);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
